SET FOREIGN_KEY_CHECKS = 0;

truncate table blogpost;

truncate table author;

truncate table comment;

truncate table author_posts;

INSERT INTO blogpost(post_id, title, content, date_created)
    VALUES(41, 'Title post 1', 'Post Content 1', "2021-06-03T12:31:18"),
        (42, 'Title post 2', 'Post Content 2', "2021-06-03T12:51:18"),
        (43, 'Title post 3', 'Post Content 3', "2021-06-03T11:31:18"),
        (44, 'Title post 4', 'Post Content 4', "2021-06-03T10:21:18"),
        (45, 'Title post 5', 'Post Content 5', "2021-06-03T12:11:18");

SET FOREIGN_KEY_CHECKS = 1;